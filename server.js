var express = require('express');
var app = express();
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;

// set the view engine to twig
app.set('view engine', 'twig');

// make express look in the public directory for assets (css/js/img)
app.use(express.static(__dirname + '/public'));

// set the home page route
app.get('/', function(req, res) {
    // ejs render automatically looks in the views folder
    res.render('index', {'title':'SkyDRM Apple','clientId':'com.herokuapp.alautest','state':'1h6fb7j4nh1h6fb7j4nh1h6fb7j4nh','redirectUrl':'https://alautest.herokuapp.com/redirect', 'scopes':'name email'});
});

app.post('/redirect', function(req, res) {
    let state = req.body.state;
    let code = req.body.code;
    let idToken = req.body.id_token;
    let userObject = req.body.user;

    let jwtPayload = JSON.stringify(jwt.decode(idToken, {complete: true}).payload);
    let jwtHeader = JSON.stringify(jwt.decode(idToken, {complete: true}).header);

    res.render('redirect', {'sentState':'1h6fb7j4nh1h6fb7j4nh1h6fb7j4nh', 'receivedState': state, 'code': code, 'idToken': idToken, 'userObject': userObject, 'jwtHeader': jwtHeader, 'jwtPayload': jwtPayload});
});

app.listen(port, function() {
    console.log('Our app is running on http://localhost:' + port);
});